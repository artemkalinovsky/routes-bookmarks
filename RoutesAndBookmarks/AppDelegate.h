//
//  AppDelegate.h
//  RoutesAndBookmarks
//
//  Created by  Artem Kalinovsky on 12/8/14.
//  Copyright (c) 2014  Artem Kalinovsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

