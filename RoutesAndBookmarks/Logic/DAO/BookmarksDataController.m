//
//  BookmarksDataController.m
//  RoutesAndBookmarks
//
//  Created by  Artem Kalinovsky on 12/10/14.
//  Copyright (c) 2014  Artem Kalinovsky. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "BookmarksDataController.h"
#import "LocalBookmarksStore.h"
#import "Bookmark.h"

@interface BookmarksDataController()

@property (strong, nonatomic, readwrite) NSArray *bookmarks;

@end

@implementation BookmarksDataController

- (NSArray *)bookmarks
{
    return [self fetchBookmarksFromLocalStore];
}

+ (instancetype)sharedManager
{
    static BookmarksDataController *bookmarksDataController = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        bookmarksDataController = [[self alloc] init];
    });

    return bookmarksDataController;
}

- (NSArray *)fetchBookmarksFromLocalStore
{
    NSArray *bookmarks = [[[LocalBookmarksStore sharedManager] fetchBookmarksAndSortBy:@"name"] fetchedObjects];
    return bookmarks;
}

- (void)deleteBookmark:(Bookmark *)bookmark
{
    [[LocalBookmarksStore sharedManager].managedObjectContext deleteObject:bookmark];
    [[LocalBookmarksStore sharedManager].managedObjectContext save:nil];
}

- (void)saveChanges
{
    [[LocalBookmarksStore sharedManager].managedObjectContext save:nil];
}

@end
