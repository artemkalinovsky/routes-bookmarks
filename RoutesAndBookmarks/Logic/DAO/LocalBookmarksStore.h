//
//  LocalBookmarksStore.h
//  RoutesAndBookmarks
//
//  Created by  Artem Kalinovsky on 12/10/14.
//  Copyright (c) 2014  Artem Kalinovsky. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NSManagedObjectContext;
@class NSFetchedResultsController;

@interface LocalBookmarksStore : NSObject

@property (strong, nonatomic, readonly) NSManagedObjectContext *managedObjectContext;

+ (instancetype)sharedManager;
- (NSFetchedResultsController *)fetchBookmarksAndSortBy:(NSString *)fieldName;

@end
