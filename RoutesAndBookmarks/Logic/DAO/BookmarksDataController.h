//
//  BookmarksDataController.h
//  RoutesAndBookmarks
//
//  Created by  Artem Kalinovsky on 12/10/14.
//  Copyright (c) 2014  Artem Kalinovsky. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Bookmark;

@interface BookmarksDataController : NSObject

@property (strong, nonatomic, readonly) NSArray *bookmarks;

+ (instancetype)sharedManager;
- (void)deleteBookmark:(Bookmark *)bookmark;
- (void)saveChanges;

@end
