//
// Created by  Artem Kalinovsky on 12/18/14.
// Copyright (c) 2014  Artem Kalinovsky. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import "AFNetworking.h"
#import "FoursquareWebService.h"
#import "FoursquareWebServiceConstants.h"
#import "SVProgressHUD.h"
#import "Bookmark.h"

@interface FoursquareWebService()
@property (strong, nonatomic) AFHTTPRequestOperation *afhttpRequestOperation;
@end

@implementation FoursquareWebService

- (void)fetchNearbyPlacesForBookmark:(Bookmark *)bookmark
                         withSuccess:(FoursquareWebServiceSuccessResponse)proceedWithServerResponse
                           orFailure:(FoursquareWebServiceFailure)proceedWithError
{
    NSDictionary *params = @{
            @"client_id" : kFoursquareClientId,
            @"client_secret" : kFoursquareClientSecret,
            @"v" : kFoursquareAPIVersion,
            @"ll" : [NSString stringWithFormat:@"%f,%f", bookmark.location.coordinate.latitude, bookmark.location.coordinate.longitude]
    };

    NSURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"GET"
                                                                          URLString:kFoursquareSearchAPIURL
                                                                         parameters:params
                                                                              error:nil];

    self.afhttpRequestOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    self.afhttpRequestOperation.responseSerializer = [AFJSONResponseSerializer serializer];

     __weak FoursquareWebService* weakSelf = self;
    
    [self.afhttpRequestOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     
     {
         NSArray *venuesResponse = responseObject[@"response"][@"venues"];
         NSArray *venues = [weakSelf fetchFoursquareVenuesFromServerResponse:venuesResponse];
         dispatch_async(dispatch_get_main_queue(), ^{
             proceedWithServerResponse(venues);
             [SVProgressHUD dismiss];
         });
     }
                                                       failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             [SVProgressHUD dismiss];
             proceedWithError(error);
         });
     }];
    
    [[NSOperationQueue mainQueue] addOperation:self.afhttpRequestOperation];
    [SVProgressHUD show];
    [self.afhttpRequestOperation resume];
}

- (NSArray *)fetchFoursquareVenuesFromServerResponse:(NSArray *)venuesResponse
{
    NSMutableArray *foursquareVenues = [[NSMutableArray alloc] init];
    
    for(NSDictionary *venueDict in venuesResponse)
    {
        @try
        {
            [foursquareVenues addObject:venueDict[@"name"]];
        }
        @catch (NSException *e)
        {

        }
    }
    return foursquareVenues;
}

- (void)cancelRequest
{
    [SVProgressHUD dismiss];
    if(self.afhttpRequestOperation.isExecuting)
    {
        [self.afhttpRequestOperation cancel];
        self.afhttpRequestOperation = nil;
    }
}

@end