//
// Created by  Artem Kalinovsky on 12/18/14.
// Copyright (c) 2014  Artem Kalinovsky. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef void (^ FoursquareWebServiceSuccessResponse)(NSArray *);
typedef void (^ FoursquareWebServiceFailure)(NSError *);

@class Bookmark;

@interface FoursquareWebService : NSObject

- (void)fetchNearbyPlacesForBookmark:(Bookmark *)bookmark
                         withSuccess:(FoursquareWebServiceSuccessResponse)proceedWithServerResponse
                           orFailure:(FoursquareWebServiceFailure)proceedWithError;
- (void)cancelRequest;

@end