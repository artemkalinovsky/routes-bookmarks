//
// Created by  Artem Kalinovsky on 12/18/14.
// Copyright (c) 2014  Artem Kalinovsky. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString* const kFoursquareSearchAPIURL;
extern NSString* const kFoursquareClientId;
extern NSString* const kFoursquareClientSecret;
extern NSString* const kFoursquareAPIVersion;