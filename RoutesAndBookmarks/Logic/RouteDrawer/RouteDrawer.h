//
// Created by  Artem Kalinovsky on 12/17/14.
// Copyright (c) 2014  Artem Kalinovsky. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MKMapView, Bookmark;
@protocol RouteDrawerDelegate;

@interface RouteDrawer : NSObject

@property (strong, nonatomic, readonly) Bookmark *destinationBookmark;
@property (weak, nonatomic) id<RouteDrawerDelegate> delegate;

- (instancetype)initForMap:(MKMapView *)mapView;

- (void)dismiss;
- (void)showRouteToBookmark:(Bookmark *)bookmark;

@end

@protocol RouteDrawerDelegate
@required
- (void)routeCalculationFailedWithError:(NSError *)error;

@end