//
// Created by  Artem Kalinovsky on 12/17/14.
// Copyright (c) 2014  Artem Kalinovsky. All rights reserved.
//


#import <MapKit/MapKit.h>
#import "RouteDrawer.h"
#import "SVProgressHUD.h"
#import "Bookmark.h"

@interface RouteDrawer()
@property (weak, nonatomic) MKMapView *mapView;
@property (strong, nonatomic) MKDirections *currentRouteDirections;
@property (strong, nonatomic, readwrite) Bookmark *destinationBookmark;
@end

@implementation RouteDrawer

- (instancetype)initForMap:(MKMapView *)mapView
{
    self = [super init];
    if (self)
    {
        _mapView = mapView;
    }
    return self;
}

- (void)dismiss
{
    if([self.currentRouteDirections isCalculating])
    {
        [self.currentRouteDirections cancel];
    }
    [self.mapView removeAnnotations:self.mapView.annotations];
    [self.mapView removeOverlays:self.mapView.overlays];
    self.currentRouteDirections = nil;
    self.destinationBookmark = nil;
}

- (void)showRouteToBookmark:(Bookmark *)bookmark
{
    [self dismiss];
    [self.mapView addAnnotation:bookmark];
    self.destinationBookmark = bookmark;
    [self fetchDirectionsToDestinationBookmark:bookmark];
}

- (void)fetchDirectionsToDestinationBookmark:(Bookmark *)bookmark
{
    if(!self.mapView.userLocation.location)
    {
        NSDictionary *details = @{NSLocalizedDescriptionKey : @"Location is disabled"};
        NSError *error = [NSError errorWithDomain:@"routeDrawer" code:500 userInfo:details];
        [self.delegate routeCalculationFailedWithError:error];
        return;
    }

    MKDirectionsRequest *request = [[MKDirectionsRequest alloc] init];
    request.source = [MKMapItem mapItemForCurrentLocation];
    request.requestsAlternateRoutes = NO;

    CLLocationCoordinate2D destinationCoordinate = bookmark.location.coordinate;

    MKPlacemark *destination = [[MKPlacemark alloc] initWithCoordinate:destinationCoordinate
                                                     addressDictionary:nil];

    request.destination = [[MKMapItem alloc] initWithPlacemark:destination];
    request.transportType = MKDirectionsTransportTypeAny;

    self.currentRouteDirections = [[MKDirections alloc] initWithRequest:request];
    [SVProgressHUD show];
    [self.currentRouteDirections calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse *response, NSError *error)
    {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (error)
                    {
                        [self.delegate routeCalculationFailedWithError:error];
                        [SVProgressHUD dismiss];
                    }
                    else
                    {
                        [self drawRoute:response];
                        [SVProgressHUD dismiss];
                    }
                });
    }];
}

-(void)drawRoute:(MKDirectionsResponse *)response
{
    MKMapRect totalRect = MKMapRectNull;
    for (MKRoute *route in response.routes)
    {
        [self.mapView addOverlay:route.polyline level:MKOverlayLevelAboveRoads];

        MKPolygon *polygon = [MKPolygon polygonWithPoints:route.polyline.points count:route.polyline.pointCount];
        MKMapRect routeRect = [polygon boundingMapRect];
        totalRect = MKMapRectUnion(totalRect, routeRect);
    }
    [self.mapView setVisibleMapRect:totalRect edgePadding:UIEdgeInsetsMake(30, 30, 30, 30) animated:YES];
}

@end