//
//  Bookmark.h
//  RoutesAndBookmarks
//
//  Created by  Artem Kalinovsky on 12/10/14.
//  Copyright (c) 2014  Artem Kalinovsky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <MapKit/MapKit.h>

@class CLLocation;

@interface Bookmark : NSManagedObject  <MKAnnotation>

@property (nonatomic, assign) CLLocation *location;
@property (nonatomic, retain) NSString *name;

- (instancetype)initWithCoordinates:(CLLocationCoordinate2D)paramCoordinates title:(NSString *)paramTitle;

@end
