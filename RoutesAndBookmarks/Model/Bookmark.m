//
//  Bookmark.m
//  RoutesAndBookmarks
//
//  Created by  Artem Kalinovsky on 12/10/14.
//  Copyright (c) 2014  Artem Kalinovsky. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import "Bookmark.h"
#import "LocalBookmarksStore.h"

@implementation Bookmark

@dynamic location;
@dynamic name;

- (instancetype) initWithCoordinates:(CLLocationCoordinate2D)paramCoordinates title:(NSString *)paramTitle
{
    NSManagedObjectContext *managedObjectContext = [LocalBookmarksStore sharedManager].managedObjectContext;
    self = [NSEntityDescription insertNewObjectForEntityForName:@"Bookmark" inManagedObjectContext:managedObjectContext];

    self.name=paramTitle;
    CLLocation *loc = [[CLLocation alloc] initWithLatitude:paramCoordinates.latitude longitude:paramCoordinates.longitude];
    self.location = loc;

    [managedObjectContext save:nil];
    return self;
}

- (CLLocationCoordinate2D)coordinate
{
    return self.location.coordinate;
}

- (NSString *)title
{
    return self.name;
}

@end
