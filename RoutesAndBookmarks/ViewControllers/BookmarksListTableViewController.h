//
//  BookmarksListTableViewController.h
//  RoutesAndBookmarks
//
//  Created by  Artem Kalinovsky on 12/9/14.
//  Copyright (c) 2014  Artem Kalinovsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookmarksListTableViewController : UITableViewController

@end
