//
//  MainScreenViewController.h
//  RoutesAndBookmarks
//
//  Created by  Artem Kalinovsky on 12/9/14.
//  Copyright (c) 2014  Artem Kalinovsky. All rights reserved.
//
#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0f)

extern NSString* const DestinationBookmarkHasBeenDeletedNotification;
extern NSString* const BuildRouteForBookmarkFromDetailsViewNotification;
extern NSString* const CentersBookmarkFromDetailsViewNotification;

typedef NS_ENUM(NSInteger, ApplicationMode)
{
    ApplicationModeNormal,
    ApplicationModeRouting
};


#import <MapKit/MapKit.h>

@protocol BookmarksPopoverDelegate, RouteDrawerDelegate, WYPopoverControllerDelegate;

@interface MainScreenViewController : UIViewController <MKMapViewDelegate, CLLocationManagerDelegate, BookmarksPopoverDelegate, RouteDrawerDelegate,WYPopoverControllerDelegate>

@end
