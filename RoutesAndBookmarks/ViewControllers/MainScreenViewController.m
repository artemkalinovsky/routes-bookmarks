//
//  MainScreenViewController.m
//  RoutesAndBookmarks
//
//  Created by  Artem Kalinovsky on 12/9/14.
//  Copyright (c) 2014  Artem Kalinovsky. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import <CoreData/CoreData.h>
#import "BookmarksPopoverViewController.h"
#import "RouteDrawer.h"
#import "WYPopoverController.h"
#import "MainScreenViewController.h"
#import "Bookmark.h"
#import "BookmarksDataController.h"
#import "WYStoryboardPopoverSegue.h"
#import "BookmarkDetailsViewController.h"

NSString* const DestinationBookmarkHasBeenDeletedNotification = @"DestinationBookmarkHasBeenDeletedNotification";
NSString* const BuildRouteForBookmarkFromDetailsViewNotification = @"BuildRouteForBookmarkFromDetailsViewNotification";
NSString* const CentersBookmarkFromDetailsViewNotification = @"CentersBookmarkFromDetailsViewNotification";

@interface MainScreenViewController ()

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) IBOutlet UILongPressGestureRecognizer *longTapOnMapRecognizer;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *routeBarButtonItem;
@property (assign, nonatomic) ApplicationMode applicationMode;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) WYPopoverController *bookmarksPopover;
@property (strong, nonatomic) RouteDrawer *routeDrawer;
@property (strong, nonatomic) Bookmark *selectedBookmark;

- (IBAction)longTapOnMap:(UILongPressGestureRecognizer *)sender;
- (IBAction)tapOnRouteBarButtonItem:(UIBarButtonItem *)sender;

@end

@implementation MainScreenViewController

- (CLLocationManager *)locationManager
{
    if(!_locationManager)
    {
        _locationManager = [[CLLocationManager alloc] init];
    }
    return _locationManager;
}

- (void)subscribeOnNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(destinationBookmarkDeletedNotification:)
                                                 name:DestinationBookmarkHasBeenDeletedNotification
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(buildRouteForBookmarkFromDetailsViewNotification:)
                                                 name:BuildRouteForBookmarkFromDetailsViewNotification
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(centerMapWithBookmarkFromNotification:)
                                                 name:CentersBookmarkFromDetailsViewNotification
                                               object:nil];
}

- (void)unsubscribeFromNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:DestinationBookmarkHasBeenDeletedNotification
                                                  object:nil];

    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:BuildRouteForBookmarkFromDetailsViewNotification
                                                  object:nil];

    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:CentersBookmarkFromDetailsViewNotification
                                                  object:nil];
}

#pragma mark - NSObject
- (void)dealloc
{
    [self unsubscribeFromNotifications];
}

#pragma mark - UIViewController
- (void)viewDidLoad
{
    [super viewDidLoad];

    self.mapView.delegate = self;
    self.locationManager.delegate = self;
    if(IS_OS_8_OR_LATER)
    {
        [self.locationManager requestWhenInUseAuthorization];
        [self.locationManager requestAlwaysAuthorization];
    }
    [self.locationManager startUpdatingLocation];
    self.mapView.showsUserLocation = YES;
    self.mapView.showsPointsOfInterest = YES;

    [self.mapView addAnnotations:[BookmarksDataController sharedManager].bookmarks];

    self.routeDrawer = [[RouteDrawer alloc] initForMap:self.mapView];
    self.routeDrawer.delegate = self;
    [self subscribeOnNotifications];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showBookmarksList"])
    {
    }
    else if([segue.identifier isEqualToString:@"showBookmarksListPopover"])
    {
        BookmarksPopoverViewController *destinationViewController = (BookmarksPopoverViewController *)segue.destinationViewController;
        destinationViewController.bookmarks = [BookmarksDataController sharedManager].bookmarks;
        destinationViewController.delegate = self;
        destinationViewController.preferredContentSize = CGSizeMake(280, 280);
        
        WYStoryboardPopoverSegue *popoverSegue = (WYStoryboardPopoverSegue *)segue;

        self.bookmarksPopover = [popoverSegue popoverControllerWithSender:sender
                                                 permittedArrowDirections:WYPopoverArrowDirectionAny
                                                                 animated:YES];
        self.bookmarksPopover.delegate = self;
    }
    else if ([segue.identifier isEqualToString:@"showBookmarkDetails"])
    {
        BookmarkDetailsViewController *destinationViewController = (BookmarkDetailsViewController *)segue.destinationViewController;
        destinationViewController.bookmark = self.selectedBookmark;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - MKMapViewDelegate
- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id <MKOverlay>)overlay
{
    MKPolylineRenderer *renderer = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
    renderer.strokeColor = [UIColor colorWithRed:0.29 green:0.53 blue:0.91 alpha:1.0];
    renderer.lineWidth = 5.0;
    return renderer;
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)aUserLocation
{
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta = 0.005;
    span.longitudeDelta = 0.005;
    CLLocationCoordinate2D location;
    location.latitude = self.mapView.userLocation.location.coordinate.latitude;
    location.longitude = self.mapView.userLocation.location.coordinate.longitude;
    region.span = span;
    region.center = location;
    [self.mapView setRegion:region animated:YES];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if([annotation isKindOfClass:[MKUserLocation class]])
    {
        return [mapView viewForAnnotation:annotation];
    }
    MKAnnotationView *annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"BookmarkIdentifier"];
    annotationView.canShowCallout = YES;
    annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    
    return annotationView;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    if([view.annotation isKindOfClass:[Bookmark class]])
    {
        self.selectedBookmark = (Bookmark *)view.annotation;
        [self performSegueWithIdentifier:@"showBookmarkDetails" sender:self];
    }
}


#pragma mark - Application Mode Switchers
- (void)switchAppToRoutingModeWithBookmark:(Bookmark *)bookmark
{
    self.applicationMode = ApplicationModeRouting;
    [self.routeBarButtonItem setTitle:@"Clear Route"];
    [self.routeDrawer showRouteToBookmark:bookmark];
}

- (void)switchAppToNormalMode
{
    self.applicationMode = ApplicationModeNormal;
    [self.routeBarButtonItem setTitle:@"Route"];
    [self.routeDrawer dismiss];
    [self.mapView addAnnotations:[BookmarksDataController sharedManager].bookmarks];
}

#pragma mark - BookmarksPopoverDelegate
- (void)bookmarksPopover:(BookmarksPopoverViewController *)popover didSelectBookmark:(Bookmark *)bookmark
{
    [self.bookmarksPopover dismissPopoverAnimated:YES];
    [self switchAppToRoutingModeWithBookmark:bookmark];
}

#pragma mark - RouteDrawerDelegate
- (void)routeCalculationFailedWithError:(NSError *)error
{
    UIAlertView *alertView = [[UIAlertView alloc] init];
    [alertView setTitle:@"Error!"];
    [alertView setMessage:error.localizedDescription];
    [alertView addButtonWithTitle:@"OK"];
    [alertView setDelegate:self];
    [alertView show];
    [self switchAppToNormalMode];
}

#pragma mark - NSNotification Selectors
- (void)destinationBookmarkDeletedNotification:(NSNotification *)notification
{
    if(notification.object && [notification.object isKindOfClass:[Bookmark class]])
    {
        Bookmark *notificationBookmark = (Bookmark *)notification.object;
        BOOL isDestinationBookmarkDeleted = [self.routeDrawer.destinationBookmark isEqual:notificationBookmark];
        if(self.applicationMode == ApplicationModeRouting && isDestinationBookmarkDeleted)
        {
            [self switchAppToNormalMode];
        }
    }
}

- (void)buildRouteForBookmarkFromDetailsViewNotification:(NSNotification*)notification
{
    if(notification.object && [notification.object isKindOfClass:[Bookmark class]])
    {
        [self switchAppToRoutingModeWithBookmark:notification.object];
    }
}

- (void)centerMapWithBookmarkFromNotification:(NSNotification *)notification
{
    if(notification.object && [notification.object isKindOfClass:[Bookmark class]])
    {
        Bookmark *bookmark = notification.object;
        if(self.applicationMode != ApplicationModeNormal)
        {
            [self switchAppToNormalMode];
        }
        CLLocationCoordinate2D centerCoordinate = CLLocationCoordinate2DMake(bookmark.location.coordinate.latitude, bookmark.location.coordinate.longitude);
        MKCoordinateRegion region = self.mapView.region;
        region.center = centerCoordinate;
        [self.mapView setRegion:region animated:YES];
    }
}

#pragma mark - IBActions
- (IBAction)longTapOnMap:(UILongPressGestureRecognizer *)sender
{
    if(self.applicationMode == ApplicationModeNormal)
    {
        if (sender.state != UIGestureRecognizerStateEnded)
        {
            return;
        }

        CGPoint touchPoint = [sender locationInView:self.mapView];
        CLLocationCoordinate2D touchMapCoordinate = [self.mapView convertPoint:touchPoint toCoordinateFromView:self.mapView];
        Bookmark *bookmark = [[Bookmark alloc] initWithCoordinates:touchMapCoordinate
                                                             title:@"Unnamed"];
        [self.mapView addAnnotation:bookmark];
    }
}

- (IBAction)tapOnRouteBarButtonItem:(UIBarButtonItem *)sender
{
    switch (self.applicationMode)
    {
        case ApplicationModeNormal:
            if([[BookmarksDataController sharedManager].bookmarks count] > 0)
            {
                [self performSegueWithIdentifier:@"showBookmarksListPopover" sender:sender];
            }
            else
            {
                UIAlertView *alertView = [[UIAlertView alloc] init];
                [alertView setTitle:@"Information"];
                [alertView setMessage:@"Add some bookmarks at first."];
                [alertView addButtonWithTitle:@"OK"];
                [alertView setDelegate:self];
                [alertView show];
            }
            break;
        case ApplicationModeRouting:
            [self switchAppToNormalMode];
            break;
    }

}

@end
