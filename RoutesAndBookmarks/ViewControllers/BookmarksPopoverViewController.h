//
//  BookmarksPopoverViewControllerTableViewController.h
//  RoutesAndBookmarks
//
//  Created by  Artem Kalinovsky on 12/15/14.
//  Copyright (c) 2014  Artem Kalinovsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Bookmark;
@protocol BookmarksPopoverDelegate;

@interface BookmarksPopoverViewController : UITableViewController

@property (strong, nonatomic) NSArray *bookmarks;
@property (weak, nonatomic) id <BookmarksPopoverDelegate> delegate;

@end

@protocol BookmarksPopoverDelegate
@required
- (void)bookmarksPopover:(BookmarksPopoverViewController *)popover didSelectBookmark:(Bookmark *)bookmark;

@end