//
//  BookmarksListTableViewController.m
//  RoutesAndBookmarks
//
//  Created by  Artem Kalinovsky on 12/9/14.
//  Copyright (c) 2014  Artem Kalinovsky. All rights reserved.
//

#import "BookmarksListTableViewController.h"
#import "BookmarkDetailsViewController.h"
#import "Bookmark.h"
#import "BookmarksDataController.h"
#import "RouteDrawer.h"
#import "WYPopoverController.h"
#import "BookmarksPopoverViewController.h"
#import "MainScreenViewController.h"

@implementation BookmarksListTableViewController

#pragma mark - UIViewController
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void) viewWillAppear:(BOOL)animated
{
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    if ([segue.identifier isEqualToString:@"showBookmarkDetails"])
    {
        BookmarkDetailsViewController *destinationViewController = segue.destinationViewController;
        destinationViewController.bookmark = [BookmarksDataController sharedManager].bookmarks[(NSUInteger)indexPath.row];
    }
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[BookmarksDataController sharedManager].bookmarks count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BookmarkCellIdentifier" forIndexPath:indexPath];
    Bookmark *bookmark = [BookmarksDataController sharedManager].bookmarks[(NSUInteger)indexPath.row];
    cell.textLabel.text = bookmark.name;
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        Bookmark *bookmark = [BookmarksDataController sharedManager].bookmarks[(NSUInteger)indexPath.row];
        [[BookmarksDataController sharedManager] deleteBookmark:bookmark];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:DestinationBookmarkHasBeenDeletedNotification
                                                            object:bookmark];
        [tableView deleteRowsAtIndexPaths:@[indexPath]
                         withRowAnimation:UITableViewRowAnimationFade];
    }
}

@end
