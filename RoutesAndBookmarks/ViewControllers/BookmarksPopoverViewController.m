//
//  BookmarksPopoverViewControllerTableViewController.m
//  RoutesAndBookmarks
//
//  Created by  Artem Kalinovsky on 12/15/14.
//  Copyright (c) 2014  Artem Kalinovsky. All rights reserved.
//

#import "BookmarksPopoverViewController.h"
#import "Bookmark.h"

@implementation BookmarksPopoverViewController

#pragma mark - UIViewController
- (void)viewDidLoad
{
    [super viewDidLoad];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.bookmarks count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BookmarkCellIdentifier" forIndexPath:indexPath];
    Bookmark *bookmark = self.bookmarks[(NSUInteger)indexPath.row];
    cell.textLabel.text = bookmark.name;
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.delegate bookmarksPopover:self didSelectBookmark:self.bookmarks[(NSUInteger)indexPath.row]];
}

@end
