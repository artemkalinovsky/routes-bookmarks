//
//  BookmarkDetailsViewController.h
//  RoutesAndBookmarks
//
//  Created by  Artem Kalinovsky on 12/9/14.
//  Copyright (c) 2014  Artem Kalinovsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Bookmark;

@interface BookmarkDetailsViewController : UIViewController <UITabBarDelegate, UIAlertViewDelegate, UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) Bookmark *bookmark;

@end

