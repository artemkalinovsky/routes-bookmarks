//
//  BookmarkDetailsViewController.m
//  RoutesAndBookmarks
//
//  Created by  Artem Kalinovsky on 12/9/14.
//  Copyright (c) 2014  Artem Kalinovsky. All rights reserved.
//

#import "BookmarkDetailsViewController.h"
#import "Bookmark.h"
#import "BookmarksDataController.h"
#import "FoursquareWebService.h"
#import "WYPopoverController.h"
#import "RouteDrawer.h"
#import "BookmarksPopoverViewController.h"
#import "MainScreenViewController.h"

@interface BookmarkDetailsViewController ()
@property (weak, nonatomic) IBOutlet UIButton *loadNearbyPlacesButton;
@property (weak, nonatomic) IBOutlet UIButton *buildRouteButton;
@property (weak, nonatomic) IBOutlet UITableView *nearbyPlacesTableView;
@property (strong, nonatomic) NSArray *venueNames;
@property (strong, nonatomic) FoursquareWebService *foursquareWebService;

- (IBAction)tapOnTrashButton:(UIButton *)sender;
- (IBAction)tapOnLoadNearbyPlacesButton:(UIButton *)sender;
- (IBAction)tapOnBuildRouteButton:(UIButton *)sender;
- (IBAction)tapOnCenterInMapViewButton:(UIButton *)sender;
@end

@implementation BookmarkDetailsViewController

- (NSArray *)venueNames
{
    if(!_venueNames)
    {
        _venueNames = [[NSArray alloc]init];
    }
    return _venueNames;
}

- (FoursquareWebService *)foursquareWebService
{
    if(!_foursquareWebService)
    {
        _foursquareWebService = [[FoursquareWebService alloc]init];
    }
    return _foursquareWebService;
}

- (void)fetchNearbyPlacesForCurrentBookmark
{
    [self.foursquareWebService fetchNearbyPlacesForBookmark:self.bookmark
                                                withSuccess:^(NSArray *venueNames)
     {
         self.venueNames= venueNames;
         [self.nearbyPlacesTableView reloadData];
         
     }
                                                  orFailure:^(NSError *error)
     {
         UIAlertView *alertView = [[UIAlertView alloc] init];
         [alertView setTitle:@"Error"];
         [alertView setMessage:error.localizedDescription];
         [alertView addButtonWithTitle:@"OK"];
         [alertView setDelegate:self];
         [alertView show];
     }];
 
}

- (void)dismissLoadNearbyPlacesButton
{
    self.loadNearbyPlacesButton.enabled = FALSE;
    self.loadNearbyPlacesButton.hidden = YES;
}

- (void)initViewForNamedBookmark
{
    self.loadNearbyPlacesButton.enabled = YES;
    self.loadNearbyPlacesButton.hidden = FALSE;
    self.nearbyPlacesTableView.hidden = YES;
}

#pragma mark - UIViewController
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = self.bookmark.name;
    self.nearbyPlacesTableView.delegate = self;
    
    if([self.bookmark.name isEqualToString:@"Unnamed"])
    {
        [self dismissLoadNearbyPlacesButton];
        [self fetchNearbyPlacesForCurrentBookmark];
    }
    else
    {
        [self initViewForNamedBookmark];
    }

}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.foursquareWebService cancelRequest];
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.venueNames count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *venueName = self.venueNames[(NSUInteger) indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PlaceCellReuseIdentifier" forIndexPath:indexPath];
    cell.textLabel.text = venueName;
    return cell;
}

#pragma mark - UITableViewDelegate
- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.title = self.bookmark.name = self.venueNames[(NSUInteger) indexPath.row];
    [[BookmarksDataController sharedManager] saveChanges];
    [self.nearbyPlacesTableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
            [[NSNotificationCenter defaultCenter] postNotificationName:DestinationBookmarkHasBeenDeletedNotification
                                                                object:self.bookmark];
            
            [[BookmarksDataController sharedManager] deleteBookmark:self.bookmark];
            [self.navigationController popViewControllerAnimated:YES];
            break;
        case 1:
            break;
        default:
            break;
    }
}

#pragma mark - IBActions
- (IBAction)tapOnTrashButton:(UIButton *)sender
{
    UIAlertView *alertView = [[UIAlertView alloc] init];
    [alertView setTitle:nil];
    [alertView setMessage:@"Delete this bookmark?"];
    [alertView addButtonWithTitle:@"Yes"];
    [alertView addButtonWithTitle:@"No"];
    [alertView setDelegate:self];
    [alertView show];
}

- (IBAction)tapOnLoadNearbyPlacesButton:(UIButton *)sender
{
    [self dismissLoadNearbyPlacesButton];
    self.nearbyPlacesTableView.hidden = FALSE;
    [self fetchNearbyPlacesForCurrentBookmark];
}

- (IBAction)tapOnBuildRouteButton:(UIButton *)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:BuildRouteForBookmarkFromDetailsViewNotification
                                                        object:self.bookmark];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)tapOnCenterInMapViewButton:(UIButton *)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:CentersBookmarkFromDetailsViewNotification
                                                        object:self.bookmark];
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
